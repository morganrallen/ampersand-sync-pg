var pgSync = require("../");

var async = require("async");
var pg = require("pg");
var test = require("tape");

var AmpersandModel = require("ampersand-model");
var DBMigrate = require( 'db-migrate' );

try {
  var dbConfig = require("../database");
} catch(e) {
  throw new Error("database.json not available. See database.json.example in project root");
}

var dbmigrate = DBMigrate.getInstance(true);

var pool = new pg.Pool(dbConfig.dev);

dbConfig.dev.idleTimeoutMillis = 1;

// setup base model with Postgres config
var DBModel = AmpersandModel.extend({
  sync: pgSync(dbConfig.dev)
});

// extend from the DB model and provide
// a table name for this one
var Person = DBModel.extend({
  tableName: "people",
  
  props: {
    id: "integer",
    email: "string",
    name: "string",
    occupation: "string"
  }
});

async.waterfall([function(done) {
  dbmigrate.down( Number.MAX_VALUE, null, done);
}, function(done) {
  dbmigrate.up( Number.MAX_VALUE, null, done);
}, function(done) {
  pool.connect(done);
}, function(client, poolDone, done) {
  var fryD = -1;

  var fry = new Person({
    name: "Philip J. Fry",
    email: "cosmicf74@planetexpress.momco",
    occupation: "Delivery Boy"
  });

  test(function(t) {
    t.plan(2);

    fry.save(null, {
      success: function() {;
        fryD = fry.getId();

        t.ok(fryD, "ID updated");
        t.pass("success");
      }
    });
  }, "create");

  var newEmail = "cosmicf75@planetexpress.momco";

  test(function(t) {
    t.plan(1);

    fry.save({
      email: newEmail
    }, {
      success: function() {
        t.equal(fry.all.email, newEmail, "success");
      }
    })
  }, "save update");

  test(function(t) {
    t.plan(3);

    client.query("SELECT * FROM people;", function(err, results) {
      t.notOk(err, "no error");
      t.equal(results.rows.length, 1, "one person in db");
      t.equal(fry.all.email, newEmail, "email update in db");
    });
  }, "database check");

  test(function(t) {
    t.plan(1);

    var fry = new Person({
      id: fryD
    });

    fry.fetch({
      success: function() {
        t.equal(fry.getId(), fryD, "id matches on fetch");
      }
    });
  }, "fetch");

  test(function(t) {
    t.plan(1);

    var fry = new Person({
      id: fryD
    });

    fry.destroy({
      success: function() {
        t.pass();
      }
    });
  }, "destroy");

  test(function(t) {
    t.plan(1);

    var noop = new Person({
      id: 606060
    });

    noop.fetch({
      error: function() {
        t.pass("failed");
      }
    });
  }, "fetch non-existant user");

  test(function(t) {
    t.plan(1);

    client.on("end", function() {
      t.pass("client ended"); 
    });

    client.end();
  }, "closing pool");
}]);
