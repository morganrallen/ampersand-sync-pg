'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, cb) {
  db.createTable("people", {
    id: {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    email: "string",
    name: "string",
    occupation: "string"
  }, cb);
};

exports.down = function(db, cb) {
  db.dropTable("people", cb);
};
