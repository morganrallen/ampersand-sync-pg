# ampersand-sync-pg

Sync Ampersand data to Postgres database

## install
```
npm install --save ampersand-sync-pg
```

## usage

#### creating model
```
  var pgSync = require("ampersand-sync-pg");

  var Person = AmpersandModel.extend({
    props: {
      name: "string",
      occupation: "string"
    },

    tableName: "users",
    sync: pgSync({
      database: "ampsync",
      idleTimeoutMillis: 1,
      user: "root",
      password: "greatpassword"
    })
  });

  var fry = new Person({
    name: "Philip J. Fry",
    occupation: "Delivery Boy"
  });

  fry.save();

```

Other Ampersand sync methods should work as expected.
