var async = require("async");
var pg = require("pg");

module.exports = function(config) {
  var pool = new pg.Pool(config);

  return sync;

  function sync(method, model, options) {
    if(!model.tableName) {
      var err = new Error("Missing tableName");

      if(options.failure instanceof Function) {
        return options.failure(err);
      } else {
        throw err;
      }
    }

    var tableName = model.tableName;
    var idAttribute = model.idAttribute;

    async.waterfall([
      function(done) {
        pool.connect(function(err, client, poolDone) {
          done(err, client, poolDone); 
        });
      },

      function(client, poolDone, done) {
        switch(method) {
          case "create":
            var keys = Object.keys(model.all);
            var dollars = "";
            for(var i = 1; i < keys.length + 1; i++)
              dollars += ("$" + i) + (i < keys.length ? ", " : "");

            client.query("INSERT INTO " + tableName + " (" + keys + ") VALUES (" + dollars + ") RETURNING *",
              Object.keys(model.all).map(function(k) {
                return model.all[k];
              }),
              function(err, result) {
                var attributes = result.rows[0];

                poolDone();

                if(attributes[idAttribute]) {
                  model.set(idAttribute, attributes[idAttribute]);
                }

                done(err, result.rows[0]);
              }
            );
          break;

          case "read":
            client.query("SELECT * FROM " + tableName + " WHERE " + idAttribute + " = $1", [ model.getId() ], function(err, result) {
              poolDone();

              if(!err && result.rows.length === 0) {
                err = new Error("User not found");
              }

              done(err, result.rows[0]);
            });
          break;

          case "delete":
            client.query("DELETE FROM " + tableName + " WHERE " + idAttribute + " = $1", [ model.getId() ], function(err, result) {
              poolDone();

              done(err, result.rows[0]);
            });
          break;

          case "update":
            var keys = Object.keys(model.all);
            var kv = "";
            for(var i = 1; i < keys.length; i++)
              kv += (keys[i] + " = $" + (i + 1)) + (i < keys.length - 1 ? ", " : "");

            var values = Object.keys(model.all).map(function(k) {
              return model.all[k];
            });

            client.query("UPDATE " + tableName + " SET " + kv + " WHERE id = $1", values, function(err, result) {
              poolDone();

              done(err, result.rows[0]);
            });
          break;

          default:
            throw new Error("Not Implemented: " + method);
        }
      }], function(err, result) {
        if(err) {
          if(options.error) options.error(result, "error", err.message);
          if(options.always) options.always(err, result);
          return;
        }

        if(options.success) return options.success(result, "success", result);
      });
  }
}
